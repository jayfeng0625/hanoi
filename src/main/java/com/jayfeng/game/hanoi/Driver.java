package com.jayfeng.game.hanoi;

import com.jayfeng.game.hanoi.core.GameState;
import com.jayfeng.game.hanoi.core.Instruction;
import com.jayfeng.game.hanoi.core.RecursiveSolver;
import com.jayfeng.game.hanoi.renderer.InstructionTextRenderer;

import java.util.concurrent.*;

public class Driver {

  public static void main(String[] args) {

    GameState gameState = GameState.getInstance();

    int totalDiskCount = 10;
    String labelA = "A";
    String labelB = "B";
    String labelC = "C";

    int minimumStepsRequired = Utils.minimumStepCount(totalDiskCount);
    System.out.println("Minimum steps required: " + minimumStepsRequired);

    BlockingQueue<Instruction> instructionsQueue = new ArrayBlockingQueue<>(minimumStepsRequired);

    InstructionTextRenderer renderer = new InstructionTextRenderer(totalDiskCount);
    RecursiveSolver recursiveSolver = new RecursiveSolver(totalDiskCount, labelA, labelB, labelC);

    CompletableFuture<Void> render = CompletableFuture.runAsync(() -> {
      while (!gameState.hasCompleted()) {
        renderer.handle(instructionsQueue);
      }
    });

    CompletableFuture<Void> compute = CompletableFuture.runAsync(() -> {
      try {
        recursiveSolver.handle(instructionsQueue);
      } catch (InterruptedException e) {
        throw new CompletionException(e);
      }
    });

    CompletableFuture.allOf(compute, render).join();
  }
}
