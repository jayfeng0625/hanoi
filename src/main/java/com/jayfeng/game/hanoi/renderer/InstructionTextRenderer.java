package com.jayfeng.game.hanoi.renderer;

import com.jayfeng.game.hanoi.Utils;
import com.jayfeng.game.hanoi.core.GameState;
import com.jayfeng.game.hanoi.core.Instruction;
import com.jayfeng.game.hanoi.core.InstructionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

public class InstructionTextRenderer implements InstructionHandler {

  private GameState gameState = GameState.getInstance();
  private AtomicInteger instructionCount = new AtomicInteger(0);

  public InstructionTextRenderer(int totalDiskCount) {
    CompletableFuture.runAsync(() -> {
      int minimumStepsRequired = Utils.minimumStepCount(totalDiskCount);
      //noinspection StatementWithEmptyBody
      while (instructionCount.get() < minimumStepsRequired) {}
      gameState.setCompleted(true);
    });
  }

  @Override
  public void handle(BlockingQueue<Instruction> instructions) {
    List<Instruction> instructionsToProcess = new ArrayList<>();

    if (instructions.drainTo(instructionsToProcess) > 0) {
      for (Instruction instruction : instructionsToProcess) {
        System.out.println("Move from " + instruction.from() + " to " + instruction.to());
        instructionCount.incrementAndGet();
      }
    }
  }
}
