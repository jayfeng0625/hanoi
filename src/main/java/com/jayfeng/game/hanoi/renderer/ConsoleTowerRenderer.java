package com.jayfeng.game.hanoi.renderer;

import com.jayfeng.game.hanoi.core.GameState;
import com.jayfeng.game.hanoi.core.Instruction;
import com.jayfeng.game.hanoi.core.InstructionHandler;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ConsoleTowerRenderer implements InstructionHandler {

  private GameState gameState = GameState.getInstance();
  private Map<String, Deque<Integer>> pegs = new ConcurrentHashMap<>();

  public ConsoleTowerRenderer(int totalDiskCount, String from, String spare, String to) {

    pegs.put(from, new ArrayDeque<>());
    pegs.put(spare, new ArrayDeque<>());
    pegs.put(to, new ArrayDeque<>());

    for (int i = 1, n = totalDiskCount + 1; i < n; i++) {
      pegs.get(from).offerFirst(i);
    }

    CompletableFuture.runAsync(() -> {
      //noinspection StatementWithEmptyBody
      while (pegs.get(to).size() < totalDiskCount) {}
      gameState.setCompleted(true);
    });
  }

  @Override
  public void handle(BlockingQueue<Instruction> instructions) {

    List<Instruction> instructionsToProcess = new ArrayList<>();

    if (instructions.drainTo(instructionsToProcess) > 0) {
      for (Instruction instruction : instructionsToProcess) {
        Deque<Integer> from = pegs.get(instruction.from());
        Deque<Integer> to = pegs.get(instruction.to());
        to.offerLast(from.pollLast());

        pegs.forEach((label, peg) -> System.out.println(label + ": " + peg.stream().map(String::valueOf).collect(Collectors.joining("-"))));
        System.out.println();
      }
    }
  }
}
