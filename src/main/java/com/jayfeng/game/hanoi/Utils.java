package com.jayfeng.game.hanoi;

public class Utils {

  public static int minimumStepCount(int totalDisk) {
    return totalDisk > 1 ? 2 * minimumStepCount(totalDisk - 1) + 1 : 1;
  }
}
