package com.jayfeng.game.hanoi.core;

import java.util.concurrent.BlockingQueue;

public interface InstructionHandler {

  void handle(BlockingQueue<Instruction> instructions) throws InterruptedException;
}
