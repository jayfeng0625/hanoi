package com.jayfeng.game.hanoi.core;

import java.util.concurrent.BlockingQueue;

public class RecursiveSolver implements InstructionHandler {

  private int totalDiskCount;
  private final String from;
  private final String to;
  private final String spare;

  public RecursiveSolver(int totalDiskCount, String from, String spare, String to) {
    this.totalDiskCount = totalDiskCount;
    this.from = from;
    this.spare = spare;
    this.to = to;
  }

  @Override
  public void handle(BlockingQueue<Instruction> instructions) throws InterruptedException {
    solve(totalDiskCount, from, to, spare, instructions);
  }

  private void solve(int disk, String from, String to, String spare, BlockingQueue<Instruction> instructions) throws InterruptedException {
    if (disk > 0) {
      solve(disk - 1, from, spare, to, instructions);
      instructions.put(new Instruction(from, to));
      solve(disk - 1, spare, to, from, instructions);
    }
  }
}
