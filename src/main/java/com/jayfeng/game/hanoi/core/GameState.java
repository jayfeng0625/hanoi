package com.jayfeng.game.hanoi.core;

import java.util.concurrent.atomic.AtomicBoolean;

public class GameState {

  private AtomicBoolean completed;

  private static GameState INSTANCE = new GameState();

  public static GameState getInstance() {
    return INSTANCE;
  }

  private GameState() {
    this.completed = new AtomicBoolean(false);
  }

  public boolean hasCompleted() {
    return completed.get();
  }

  public void setCompleted(boolean completed) {
    this.completed.set(completed);
  }
}
