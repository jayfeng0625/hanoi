package com.jayfeng.game.hanoi.core;

public class Instruction {

  private final String from;
  private final String to;

  public Instruction(String from, String to) {
    this.from = from;
    this.to = to;
  }

  public String from() {
    return from;
  }

  public String to() {
    return to;
  }

  @Override
  public String toString() {
    return "Move from " + from + " to " + to;
  }
}
